library FCN;

uses
  Udefine in 'Udefine.pas',
  UCRC32 in 'UCRC32.pas';

{$R *.res}


EXPORTS
  saveFCNFile name 'saveFCN',
  readFCNFile name 'readFCN',
  calculateCRC32Number name 'calCRC32';

begin
end.
 