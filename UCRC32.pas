unit UCRC32;

interface

uses
  Windows, SysUtils, Classes, Udefine;


type
  CRC32array=array[0..255]of Longint;

var
  crc32table:CRC32array;

procedure generateCRC32Array;
function calculateCRC32Number(filePath1:PChar):DWORD;stdcall;
procedure saveFCNFile(saveFile1,CheckFilePath:pchar);stdcall;
function readFCNFile(loadFile1:PChar):DWORD;stdcall;


implementation

procedure generateCRC32Array;
var
  i11,j22,crc:longword;
begin
  for i11:=0 to 256 do
  begin
    crc:=i11;

    for j22:=0 to 8 do
    begin
      if (crc and 1)=1 then
      begin
        crc:=(crc shr 1)xor $EDB88320;
      end
      else
      begin
        crc:=crc shr 1;
      end;
    end;
    crc32table[i11]:=crc;
  end;

end;

function calculateCRC32Number(filePath1:PChar):DWORD;
var
  crc32:DWORD;
  len:longword;
  i1:integer;
  fs:TFileStream;
  Buffer: array[1..8192] of Byte;
begin
  generateCRC32Array;
  try
    fs:=TFileStream.Create(filePath1,fmOpenRead);
    len:=fs.Read(Buffer,sizeof(Buffer));
    crc32:=$ffffffff;
    for i1:=1 to len do
    begin
      crc32:=((crc32 shr 8)and $ffffff)xor crc32table[(crc32 xor dword(Buffer[i1]))and $ff];
    end;
  finally
    fs.Free;
  end;
  Result:=crc32;
end;

procedure saveFCNFile(saveFile1,CheckFilePath:Pchar);
var
  fileStream1:TFileStream;
begin
  DeleteFile(saveFile1);
  If Not FileExists(saveFile1) Then
  Begin
    try
      fileStream1:= TFileStream.Create(saveFile1, fmCreate);
      FileHead.Version := '1.9.8.0';
      FileHead.author:='xiaobin';
      FileHead.CRC32:=calculateCRC32Number(CheckFilePath);
      Filehead.UpdateDate := Now;
      fileStream1.Write(FileHead, SizeOf(FileHead));
    finally
      fileStream1.Free;
    end;
  End;
end;

function readFCNFile(loadFile1:PChar):DWORD;stdcall;
var
  fileStream2:TFileStream;
begin
  try
    fileStream2:=TFileStream.Create(loadFile1,fmOpenRead);
    fileStream2.Read(FileHead,sizeof(FileHead));
    Result:=FileHead.CRC32;
  finally
    fileStream2.Free;
  end;
end;

end.
